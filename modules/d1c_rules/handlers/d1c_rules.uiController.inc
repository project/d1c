<?php

class d1cRulesUiController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage d1c rules.';
    $items[$this->path]['type'] = MENU_LOCAL_TASK;
    return $items;
  }

  protected function overviewTableHeaders($conditions, $rows, $additional_header = array()) {
    $additional_header = array(
      'weight' => t('Weight'),
      'entity' => t('Source entity'),
      'target' => t('Target'),
      'fields' => t('Relation fields'),
    );
    return parent::overviewTableHeaders($conditions, $rows, $additional_header);
  }

  protected function overviewTableRow($conditions, $id, $entity, $additional_cols = array()) {
    $source = d1c_load($entity->entity);
    $target = entity_get_info($entity->target_type);
    $additional_cols = array(
      'weight' => $entity->weight,
      'entity' => $source->title,
      'target' => $target['label'] . ': ' . $target['bundles'][$entity->target_bundle]['label'],
      'fields' => l(t('Mange fields'), 'admin/config/services/d1c/rules/' . $entity->id . '/fields'),
    );
    return parent::overviewTableRow($conditions, $id, $entity, $additional_cols);
  }

  public function overviewTable($conditions = array()) {
    $render = parent::overviewTable();
    usort($render['#rows'], '_d1c_sort_rows_by_weight');
    return $render;
  }

}
