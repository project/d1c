<?php

class d1cRulesController extends EntityAPIController {

  /**
   * Implements EntityAPIControllerInterface::create().
   */
  public function create(array $values = array()) {
    global $user;
    $values += array(
      'id' => '',
      'weight' => 0,
      'title' => '',
      'entity' => '',
      'target_type' => '',
      'target_bundle' => '',
      'selector' => 'Ид',
    );

    return parent::create($values);
  }

}
