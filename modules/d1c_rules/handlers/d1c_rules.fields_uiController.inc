<?php

class d1cRulesFieldsUiController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage d1c rules field.';
    unset($items[$this->path . '/list']);

    return $items;
  }

  protected function overviewTableHeaders($conditions, $rows, $additional_header = array()) {
    $additional_header = array(
      'weight' => t('Weight'),
      'entity' => t('Rule'),
      'type' => t('Data type'),
      'xml_field' => t('Field in xml'),
      'field' => t('Field'),
      'additional' => t('Related field'),
    );
    return parent::overviewTableHeaders($conditions, $rows, $additional_header);
  }

  protected function overviewTableRow($conditions, $id, $entity, $additional_cols = array()) {
    $parent = d1c_rules_load($entity->parent);

    $additional_cols = array(
      'weight' => $entity->weight,
      'entity' => $parent->title,
      'type' => d1c_get_1c_data_type($entity->data_type),
      'xml_field' => $entity->name,
      'field' => $entity->field,
      'additional' => $entity->additional?$entity->additional:'-',
    );

    $return = parent::overviewTableRow($conditions, $id, $entity, $additional_cols);
    foreach ($return as &$row) {
      $row = str_replace('%25', $entity->parent, $row);
    }
    return $return;
  }

  public function overviewTable($conditions = array()) {
    $parent_id = (int) arg(5);
    $conditions['parent'] = $parent_id;
    $render = array();
    $render['table'] = parent::overviewTable($conditions);
    usort($render['table']['#rows'], '_d1c_sort_rows_by_weight');
    $render['return_link']['#markup'] = l(t('Return to rules'), 'admin/config/services/d1c/rules');
    return $render;
  }

}
