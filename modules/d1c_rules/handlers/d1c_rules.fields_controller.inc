<?php

class d1cRulesFieldsController extends EntityAPIController {

  /**
   * Implements EntityAPIControllerInterface::create().
   */
  public function create(array $values = array()) {
    global $user;
    $values += array(
      'id' => '',
      'weight' => 0,
      'title' => '',
      'field' => '',
      'data_type' => '',
      'multiple' => 0,
      'name' => '',
      'additional' => '',
    );

    return parent::create($values);
  }

}
