<?php

/**
 * Implements hook_preprocess_d1c_order_cml().
 */
function template_process_d1c_order_cml(&$variables) {
  drupal_add_http_header('Content-Type', 'application/xml; charset=windows-1251');

  $variables['orders'] = array_map('_d1c_order_to_cp1251', $variables['orders']);
  $variables['orders'] = array_map('_d1c_order_array_to_object', $variables['orders']);

}

function _d1c_order_array_to_object($item) {
  $object_item = (object) $item;
  if (isset($object_item->products)) {
    $object_item->products = array_map('_d1c_order_array_to_object', $object_item->products);
  }

  return $object_item;
}

function _d1c_order_to_cp1251($item) {
  if (is_array($item)) {
    $item = array_map('_d1c_order_to_cp1251', $item);
  }
  else {
    $item = iconv('utf-8', 'windows-1251//IGNORE', $item);
  }

  return $item;
}
