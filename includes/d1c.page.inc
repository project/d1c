<?php

/**
 * 1c exchange main callback
 */
function d1c_main() {
  $has_checkauth = (bool) variable_get('d1c_check_auth', 1);
  $d1c = new d1c($has_checkauth);
  if ($d1c->error) {
    $error = $d1c->error;
    array_unshift($error, 'failure');
    return $error;
  }

  switch ($d1c->requestType()) {
    case 'catalog:checkauth':
    case 'sale:checkauth':
      variable_del('d1c_count');
      return array(
        'success',
        $d1c->cookie['name'],
        $d1c->cookie['value']
      );
      break;
    case 'catalog:init':
      module_invoke_all('d1c_import_init');
      return array(
        'zip=no',
        'file_limit=0'
      );
      break;
    case 'catalog:file':
      $result = $d1c->save_stream_file_date('filename');
      return $result;
      break;
    case 'catalog:import':
      return d1c_import($d1c);
      break;
    case 'sale:init':
      if (module_exists('d1c_order')) {
        module_invoke_all('d1c_export_init');
        return array(
          'zip=no',
          'file_limit=0',
          'sessid=' . session_id(),
          'version= 2.09',
        );
      }
      else {
        return 'failed';
      }
      break;
    case 'sale:query':
      if (module_exists('d1c_order')) {
        return d1c_export($d1c);
      }
      else {
        return 'failed';
      }
      break;
    case 'sale:success':
      module_invoke_all('d1c_export_success');
      return 'success';
      break;
    case 'sale:file':
      $result = $d1c->save_stream_file_date('filename');
      return $result;
      break;
  }
  return 'success';
}
