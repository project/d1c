<?php

/**
 * D1c administration auth settings form
 *
 * @see d1c_admin_auth_form_submit()
 * @see d1c_admin_auth_form_validate()
 */
function d1c_admin_auth_form($form, $form_state) {
  $form = array();
  $form['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('d1c_username', ''),
    '#required' => TRUE,
    '#description' => t('Spaces are allowed; punctuation is not allowed except for periods, hyphens, apostrophes, and underscores.'),
  );
  $password = variable_get('d1c_password', FALSE);
  $form['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#description' => t('The security considerations of the current password is not displayed'),
    '#required' => $password === FALSE,
  );
  $form['d1c_check_auth'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('d1c_check_auth', 1),
    '#title' => t('Check password'),
    '#description' => t('Enable for production'),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  return $form;
}

/**
 * D1c settings form validate callback
 *
 * @see d1c_admin_auth_form()
 * @see d1c_admin_auth_form_submit()
 */
function d1c_admin_auth_form_validate(&$form, &$form_state) {

  //Check for valid username inputed
  if ($error = user_validate_name($form_state['values']['username'])) {
    form_set_error('username', $error);
  }
}

/**
 * D1c settings form submit callback
 *
 * @see d1c_admin_auth_form()
 * @see d1c_admin_auth_form_validate()
 */
function d1c_admin_auth_form_submit(&$form, &$form_state) {
  form_state_values_clean($form_state);
  $values = $form_state['values'];

  //Save password has to db
  if ($values['password']) {
    $password_hash = hash('sha256', $values['password'], false);
    variable_set('d1c_password', $password_hash);
  }

  //Save username to db
  variable_set('d1c_username', $values['username']);
  variable_set('d1c_check_auth', $values['d1c_check_auth']);
  drupal_set_message(t('Settings has ben saved'));
}

/**
 * D1c administration properties settings form
 *
 * @see d1c_admin_props_form_submit()
 * @see d1c_admin_props_form_validate()
 */
function d1c_admin_props_form($form, $form_state, $op = 'add', $id = FALSE) {
  $form = array();
  if ($id !== FALSE) {
    $property = d1c_property_get_by_id($id);
  }
  else {
    $property = FALSE;
  }
  $form['op'] = array(
    '#type' => 'value',
    '#value' => $op,
  );
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $id,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Property name'),
    '#required' => TRUE,
    '#default_value' => $property ? $property->name : '',
  );
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Property type'),
    '#required' => TRUE,
    '#default_value' => $property ? $property->type : '',
    '#options' => array(
      'term' => t('Taxonomy term'),
      'value' => t('Custom value'),
    ),
  );
  $vocabularies = taxonomy_vocabulary_get_names();
  $options = array();
  foreach ($vocabularies as $vocabulary_machin_name => $vocabulary) {
    $options[$vocabulary_machin_name] = $vocabulary->name;
  }
  $options['#__NEW__#'] = t('Create new vocabulary');
  $form['vocabulary'] = array(
    '#type' => 'select',
    '#required' => TRUE,
    '#options' => $options,
    '#title' => t('Select vocabulary'),
    '#empty_option' => t('Select'),
    '#empty_value' => '_none',
    '#default_value' => $property ? $property->vocabulary : '_none',
    '#states' => array(
      'visible' => array(
        ':input[name="type"]' => array('value' => 'term')
      )
    ),
  );

  $form['vocabulary_name'] = array(
    '#title' => t('Vocabulary name'),
    '#type' => 'textfield',
    '#states' => array(
      'visible' => array(
        ':input[name="vocabulary"]' => array('value' => '#__NEW__#'),
      ),
    ),
  );
  $form['vocabulary_machine_name'] = array(
    '#type' => 'machine_name',
    '#maxlength' => 21,
    '#required' => FALSE,
    '#machine_name' => array(
      'exists' => 'taxonomy_vocabulary_machine_name_load',
      'source' => array('vocabulary_name'),
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * D1c properties settings form validate callback
 *
 * @see d1c_admin_props_form()
 * @see d1c_admin_props_form_submit()
 */
function d1c_admin_props_form_validate(&$form, &$form_state) {
  $values = $form_state['values'];
  $name = $values['name'];
  $id = $values['id'];
  $propery = d1c_property_get_by_name($name);
  if ($propery && $propery->id != $id) {
    form_set_error('name', t('Property with this name is exist.'));
  }
  if ($values['vocabulary'] == '#__NEW__#' && (empty($values['vocabulary_machine_name']) || empty($values['vocabulary_name']))) {
    form_set_error('vocabulary_machine_name', t('Field vocabulary name required.'));
  }
}

/**
 * D1c settings properties form submit callback
 *
 * @see d1c_admin_props_form()
 * @see d1c_admin_props_form_validate()
 */
function d1c_admin_props_form_submit(&$form, &$form_state) {
  form_state_values_clean($form_state);
  $values = $form_state['values'];
  unset($values['op']);
  $id = $values['id'];
  $data = variable_get('d1c_props', array());
  if ($values['type'] == 'term') {
    $vocabulary_name = $values['vocabulary'];
    if ($vocabulary_name == '#__NEW__#') {
      $vocabulary = new stdClass();
      $vocabulary->name = $values['vocabulary_name'];
      $vocabulary->machine_name = $values['vocabulary_machine_name'];
      taxonomy_vocabulary_save($vocabulary);
      $vocabulary_name = $vocabulary->machine_name;
      $values['vocabulary'] = $vocabulary_name;
    }

    d1c_create_relation_field('taxonomy_term', $vocabulary_name);
  }
  else {
    $values['vocabulary'] = NULL;
  }
  // Remove vocabulary information from values..
  unset($values['vocabulary_name'], $values['vocabulary_machine_name']);

  if ($id !== FALSE) {
    $data[$id] = (object) $values;
  }
  else {
    $data[] = (object) $values;
    end($data);
    $id = key($data);
    $data[$id]->id = $id;
  }
  if (empty($data[$id]->d1c_ids)) {
    $data[$id]->d1c_ids = array();
  }
  variable_set('d1c_props', $data);
  $form_state['redirect'] = 'admin/config/services/d1c/properties';
}

/**
 * Properties admin page.
 */
function d1c_admin_props_page() {
  $out = array();
  $properties = variable_get('d1c_props', array());
  if ($properties) {
    $vocabularies = taxonomy_vocabulary_get_names();
    $header = array(t('Name'), t('Type'), t('Vocabulary'), t('Actions'));
    foreach ($properties as $property) {
      $links = array(
        '#theme' => 'links',
        '#links' => array(
          'edit' => array(
            'title' => t('Edit'),
            'href' => t('admin/config/services/d1c/properties/edit/' . $property->id),
          ),
          'delete' => array(
            'title' => t('Delete'),
            'href' => t('admin/config/services/d1c/properties/delete/' . $property->id),
          )
        )
      );
      $rows[] = array(
        $property->name,
        ($property->type == 'value') ? t('Custom value') : t('Related term'),
        $property->vocabulary ? $vocabularies[$property->vocabulary]->name : t('None'),
        render($links),
      );
    }
    $out['data'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    );
  }
  return $out;
}

/**
 * Delete property form.
 */
function d1c_admin_props_delete_form($form, $form_state, $id) {
  $property = d1c_property_get_by_id($id);
  $path = 'admin/config/services/d1c/properties';

  if (!$property) {
    drupal_goto($path);
  }
  $question = t('Are you sure to delete property %name', array('%name' => $property->name));
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $id,
  );
  return confirm_form($form, $question, $path);
}

function d1c_admin_props_delete_form_submit($form, &$form_state) {
  $properties = variable_get('d1c_props', array());
  $id = $form_state['values']['id'];
  $property = $properties[$id];
  drupal_set_message(t('Property !name deleted', array('!name' => $property->name)));
  unset($properties[$form_state['values']['id']]);
  variable_set('d1c_props', $properties);
  $form_state['redirect'] = 'admin/config/services/d1c/properties';
}
