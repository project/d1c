<?php

class d1cUiController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage d1c items.';
    return $items;
  }

  protected function overviewTableHeaders($conditions, $rows, $additional_header = array()) {
    $additional_header = array(
      'weight' => t('Weight'),
      'filename' => t('File name'),
      'xml_item' => t('XML item'),
      'tree' => t('Is tree'),
    );
    return parent::overviewTableHeaders($conditions, $rows, $additional_header);
  }

  protected function overviewTableRow($conditions, $id, $entity, $additional_cols = array()) {
    $additional_cols = array(
      'weight' => $entity->weight,
      'filename' => $entity->filename,
      'xml_item' => $entity->xml_item,
      'tree' => $entity->tree ? t('Yes') : t('No'),
    );
    return parent::overviewTableRow($conditions, $id, $entity, $additional_cols);
  }

  public function overviewTable($conditions = array()) {
    $render = parent::overviewTable();
    usort($render['#rows'], '_d1c_sort_rows_by_weight');
    return $render;
  }

}
