<?php

/**
 * @file
 * Main helper class contain tools for processing data
 */
class d1c {

  public $error;
  public $cookie;
  private $settings_path = 'admin/config/services/d1c';
  private $query;
  private $username;
  private $password_hash;
  private $request;
  private $request_type;
  private $files;
  private $core = 'public://d1c/';

  public function __construct($chekAuth = TRUE) {
    $error = array();
    $this->query = drupal_get_query_parameters();
    $this->username = variable_get('d1c_username', '');
    $this->password_hash = variable_get('d1c_password', '');
    $this->cookie = variable_get('d1c_cookie', FALSE);
    if (!$this->cookie) {
      $this->cookie = array();
      $this->cookie['name'] = 'd1c_cookie_' . ceil(REQUEST_TIME / mt_rand(10, 100));
      $this->cookie['value'] = ceil(REQUEST_TIME / mt_rand(10, 100));
      variable_set('d1c_cookie', $this->cookie);
    }

    if (!$this->username || !$this->password_hash) {
      $text = 'Please set username and password in d1c !settings page';
      $error[] = t($text, array('!settings' => $this->settings_path));
      watchdog('D1C warning', $text, array('!settings' => $this->settings_path), WATCHDOG_WARNING);
    }

    if ($chekAuth && !$this->checkAuth()) {
      $text = 'Login or password incorrect';
      $error[] = t($text);
      watchdog('D1C error', $text, array(), WATCHDOG_ERROR);
    }

    $this->error = $error;
    $this->validateRequest();
  }

  public function get_query_string() {
    return implode('&', $this->query);
  }

  public function getParam($key) {
    return isset($this->query[$key]) ? $this->query[$key] : FALSE;
  }

  public function checkRequest($key, $value) {
    return isset($this->query[$key]) && $this->query[$key] == 'value';
  }

  public function checkAuth() {
    if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
      $username = $_SERVER['PHP_AUTH_USER'];
      $password = $_SERVER['PHP_AUTH_PW'];
      $password_hash = hash('sha256', $password, FALSE);
      if ($username == $this->username && $password_hash == $this->password_hash) {
        return TRUE;
      }
    }
    return FALSE;
  }

  public function requestType() {
    if (empty($this->request_type)) {
      $this->request['type'] = $this->getParam('type');
      $this->request['mode'] = $this->getParam('mode');
      if($this->request['type'] == 'reference'){
        $this->request['type'] = 'catalog';
      }
      $this->request_type = implode(':', $this->request);
    }
    return $this->request_type;
  }

  public function save_stream_file_date($filename_key) {
    $filepath = $this->getParam($filename_key);
    if (!$filepath) {
      watchdog('D1C error', 'filename key si not exists', array(), WATCHDOG_ERROR);
      $return = array('failed', 'filename key si not exists');
      return implode("\n", $return);
    }
    $file_uri = $this->createUri($filepath);
    if (!$file_uri) {
      watchdog('D1C error', 'Cannot create directory for saving file !filename, check permissions', array('filename' => $filepath), WATCHDOG_ERROR);
      $return = array('failed', 'Cannot create directory for saving file');
      return implode("\n", $return);
    }

    $contents = file_get_contents('php://input');
    $file = file_unmanaged_save_data($contents, $file_uri, FILE_EXISTS_REPLACE);

    if (!$file) {
      watchdog('D1C error', 'Cannot save file !filename, check permissions', array('filename' => $filepath), WATCHDOG_ERROR);
      $return = array('failed', 'Cannot save file ');
      return implode("\n", $return);
    }
    return 'success';
  }

  public function createUri($path, $checkdir = TRUE) {
    $uri = $this->core . $path;
    if ($checkdir) {
      $dirname = drupal_dirname($uri);
      if (file_prepare_directory($dirname, FILE_CREATE_DIRECTORY)) {
        return $uri;
      }
      else {
        return FALSE;
      }
    }
    return $uri;
  }

  private function validateRequest() {
    $request = $this->requestType();
    switch ($request) {
      case 'catalog:file':
      case 'catalog:import':
        if (!$this->getParam('filename')) {
          $this->error = array(
            'failure',
            'request=' . $request,
            'Request error: GET @filename is not exists'
          );
        }
        break;
    }
  }

}
