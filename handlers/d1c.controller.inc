<?php

class d1cController extends EntityAPIController {

  /**
   * Implements EntityAPIControllerInterface::create().
   */
  public function create(array $values = array()) {
    global $user;
    $values += array(
      'id' => '',
      'weight' => 0,
      'title' => '',
      'xml_item' => '',
      'filename' => '',
      'count' => 1,
      'skip' => 0,
      'tree' => 0,
    );

    return parent::create($values);
  }

}
